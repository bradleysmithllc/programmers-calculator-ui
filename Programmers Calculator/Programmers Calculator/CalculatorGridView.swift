//
//  CalculatorGridView.swift
//  Programmers Calculator
//
//  Created by Bradley Smith on 10/23/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import UIKit;

import CalculatorCore;

@IBDesignable
public class CalculatorGridView: UIView {
	var touchingRow: Int = -1;
	var touchingCol: Int = -1;
	
	var calculator: Calculator = CalculatorCore();

	var configuration: Configuration = Configuration();
	
	required public init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder);

		NSNotificationCenter.defaultCenter().addObserver(self, selector: "rotated", name: UIDeviceOrientationDidChangeNotification, object: nil)
	}

	public func rotated()
	{
		configuration.landscape(UIDeviceOrientationIsLandscape(UIDevice.currentDevice().orientation));
		setNeedsDisplay();
	}

	override public func drawRect(rect: CGRect)
	{
		// calculate the size of each cell
		let perspective: Perspective = configuration.activePerspective();
		
		let cellRect: CGRect = CalculatorGridView.calculateCellSize(viewRect: bounds, rows: perspective.rows(), columns: perspective.columns());

		let con = UIGraphicsGetCurrentContext();
		
		// iterate over all rows and columns
		for row: Int in 0...perspective.rows() - 1
		{
			for column: Int in 0...perspective.columns() - 1
			{
				// move the cell origin to it's new location
				var thisCellRect: CGRect = CalculatorGridView.calculateCellRect(cellRect: cellRect, row: row, column: column);

				CGContextSaveGState(con);
				CGContextClipToRect(con, thisCellRect);

				// reset the coordinates to 0 for each cell
				let tx = thisCellRect.origin.x;
				let ty = thisCellRect.origin.y;
				
				CGContextTranslateCTM(con, tx, ty);

				thisCellRect.origin.x = 0.0;
				thisCellRect.origin.y = 0.0;
				
				let touching: Bool;

				if (touchingRow == row && touchingCol == column)
				{
					touching = true;
				}
				else
				{
					touching = false;
				}

				CGContextSaveGState(con);
				drawCellBackground(con!, cellRect: thisCellRect, cell: perspective.cell(row: row, column: column), row: row, column: column, touching: touching);
				CGContextRestoreGState(con);

				CGContextSaveGState(con);
				drawCellBorder(con!, cellRect: thisCellRect, cell: perspective.cell(row: row, column: column), row: row, column: column, touching: touching);
				CGContextRestoreGState(con);

				CGContextSaveGState(con);
				drawCellText(con!, cellRect: thisCellRect, cell: perspective.cell(row: row, column: column), row: row, column: column, touching: touching);
				CGContextRestoreGState(con);

				CGContextRestoreGState(con);
			}
		}
	}

	private func drawCellBackground(context: CGContext, cellRect: CGRect, cell: CalculatorCell, row: Int, column: Int, touching: Bool)
	{
		let layer: Layer = activeLayer();

		// draw the outline around the box
		let clr: UIColor;

		if (touching)
		{
			clr = configuration.activeProfile().buttonPressedColor(buttonType: cell.buttonType(layer), layer: layer);
		}
		else
		{
			clr = configuration.activeProfile().buttonColor(buttonType: cell.buttonType(layer), layer: layer);
		}

		CGContextSetFillColorWithColor(context, clr.CGColor);
		CGContextFillRect(context, cellRect);
	}
	
	private func drawCellBorder(context: CGContext, cellRect: CGRect, cell: CalculatorCell, row: Int, column: Int, touching: Bool)
	{
		let layer: Layer = activeLayer();
		
		CGContextSetLineWidth(context, 0.25);
		
		CGContextSetStrokeColorWithColor(
			context,
			configuration.activeProfile().lineColor(
				buttonType: cell.buttonType(layer),
				layer: layer
				).CGColor
		);
		
		// draw the box around the key
		if (!cell.extendsUp(layer))
		{
			CGContextMoveToPoint(context, cellRect.origin.x, cellRect.origin.y);
			CGContextAddLineToPoint(context, cellRect.origin.x + cellRect.size.width, cellRect.origin.y);
		}
		
		if (!cell.extendsRight(layer))
		{
			CGContextMoveToPoint(context, cellRect.origin.x + cellRect.size.width, cellRect.origin.y);
			CGContextAddLineToPoint(context, cellRect.origin.x + cellRect.size.width, cellRect.origin.y + cellRect.size.height);
		}
		
		if (!cell.extendsDown(layer))
		{
			CGContextMoveToPoint(context, cellRect.origin.x + cellRect.size.width, cellRect.origin.y + cellRect.size.height);
			CGContextAddLineToPoint(context, cellRect.origin.x, cellRect.origin.y + cellRect.size.height);
		}
		
		if (!cell.extendsLeft(layer))
		{
			CGContextMoveToPoint(context, cellRect.origin.x, cellRect.origin.y + cellRect.size.height);
			CGContextAddLineToPoint(context, cellRect.origin.x, cellRect.origin.y);
		}
		
		CGContextStrokePath(context);
	}

	private func drawCellText(context: CGContext, cellRect: CGRect, cell: CalculatorCell, row: Int, column: Int, touching: Bool)
	{
		let layer: Layer = activeLayer();

		let clr: UIColor;
		
		if (touching)
		{
			clr = configuration.activeProfile().activeMnemonicColorPressed(buttonType: cell.buttonType(layer), layer: layer);
		}
		else
		{
			clr = configuration.activeProfile().activeMnemonicColor(buttonType: cell.buttonType(layer), layer: layer);
		}
		
		CGContextSetStrokeColorWithColor(context, clr.CGColor);

		// overlay the mnemonic
		let mne: String;

		if (cell.buttonType(layer) == ButtonType.display_x)
		{
			mne = String(calculator.stack().getX());
		}
		else
		{
			// get the cell id
			let cellId = cell.mnemonic(layer);

			// look it up in the calculator and get the mnemonic
			if let operation = calculator.operations()[cellId]
			{
				mne = operation.mnemonic();
			}
			else
			{
				mne = "INV";
			}
		}

		var textRect = CalculatorGridView.makeNormalFacetTextRect(cellRect);

		// reset the coordinates to 0 for each cell
		let tx = textRect.origin.x;
		let ty = textRect.origin.y;

		CGContextClipToRect(context, textRect);
		CGContextTranslateCTM(context, tx, ty);

		textRect.origin.x = 0;
		textRect.origin.y = 0;
		
		drawText(context, text: mne, attributes: [
			NSForegroundColorAttributeName : clr
			], targetRect: textRect);
	}
	
	public override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
	{
		let ft = touches.first
		let lp: CGPoint = ft!.locationInView(self);

		let perspective: Perspective = configuration.activePerspective();
		
		let cellRect: CGRect = CalculatorGridView.calculateCellSize(viewRect: bounds, rows: perspective.rows(), columns: perspective.columns());
		
		// iterate over all rows and columns
		for row: Int in 0...perspective.rows() - 1
		{
			for column: Int in 0...perspective.columns() - 1
			{
				// move the cell origin to it's new location
				let thisCellRect: CGRect = CalculatorGridView.calculateCellRect(cellRect: cellRect, row: row, column: column);
				
				if (thisCellRect.contains(lp))
				{
					touchingRow = row;
					touchingCol = column;
				}
			}
		}
	
		self.setNeedsDisplay();
		print("Began \(lp)");
	}
	
	private var _activeLayer: Layer = Layer.normal;

	private func activeLayer() -> Layer
	{
		return _activeLayer;
	}

	public override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?)
	{
		print("Moved");
	}
	
	public override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?)
	{
		// handle the operation
		let cell = configuration.activePerspective().cell(row: touchingRow, column: touchingCol);

		// check for a function key.  These do not get passed to the calculator engine
		if (cell.buttonType(activeLayer()) == ButtonType.function_a)
		{
			// switch the active layer
			_activeLayer = Layer.function_a;
		}
		else if (cell.buttonType(activeLayer()) == ButtonType.normal)
		{
			// switch the active layer
			_activeLayer = Layer.normal;
		}
		else if (cell.buttonType(activeLayer()) == ButtonType.function_b)
		{
			// switch the active layer
			_activeLayer = Layer.function_b;
		}
		else if (cell.buttonType(activeLayer()) == ButtonType.function_c)
		{
			// switch the active layer
			_activeLayer = Layer.function_c;
		}
		else if (cell.buttonType(activeLayer()) == ButtonType.function_d)
		{
			// switch the active layer
			_activeLayer = Layer.function_d;
		}
		else if (cell.buttonType(activeLayer()) == ButtonType.placeholder)
		{
			// ignore
		}
		else
		{
			let mne = cell.mnemonic(activeLayer());
			
			do
			{
				try calculator.invoke(mne);
				print("\(calculator.stack().getX())");
			}
			catch
			{
				print("\(error)");
			}
		}

		touchingRow = -1;
		touchingCol = -1;

		self.setNeedsDisplay();
		print("Ended");
	}
	
	public override func touchesCancelled(touches: Set<UITouch>?, withEvent event: UIEvent?)
	{
		print("Cancelled");
	}
	
	class func calculateCellSize(viewRect viewRect: CGRect, rows: Int, columns: Int) -> CGRect
	{
		let cPoint: CGPoint = CGPoint(x: viewRect.origin.x, y: viewRect.origin.y);
		
		let width = viewRect.width.native;
		let height = viewRect.height.native;

		let cRect: CGRect =
			CGRect(
				origin: cPoint,
				size: CGSize(
					width: CGFloat((width / CGFloat.NativeType(columns))),
					height: CGFloat((height / CGFloat.NativeType(rows)))
				)
			);

		return cRect;
	}

	class func calculateCellRect(cellRect cellRect: CGRect, row: Int, column: Int) -> CGRect
	{
		let cPoint: CGPoint = CGPoint(
			x: CGFloat(cellRect.origin.x.native + (CGFloat.NativeType(column) * cellRect.width.native)),
			y: CGFloat(cellRect.origin.y.native + (CGFloat.NativeType(row) * cellRect.height.native))
		);

		let cRect: CGRect =
		CGRect(
			origin: cPoint,
			size: CGSize(
				width: cellRect.width,
				height: cellRect.height
			)
		);

		return cRect;
	}

	class func makeNormalFacetTextRect(cellRect: CGRect) -> CGRect
	{
		// new origin
		let new_x = cellRect.size.width * UILayoutConstants.NORMAL_FACET_LEFT_MARGIN;
		let new_y = UILayoutConstants.NORMAL_FACET_TOP_BAR_PERCENT * cellRect.size.height;

		// new width.  subtract the two margins from the width
		let new_width = cellRect.size.width * (1.0 - (UILayoutConstants.NORMAL_FACET_LEFT_MARGIN + UILayoutConstants.NORMAL_FACET_RIGHT_MARGIN));

		// new height.  subtract the two margins from the height
		let new_height = cellRect.size.height * (1.0 - (UILayoutConstants.NORMAL_FACET_TOP_BAR_PERCENT + UILayoutConstants.NORMAL_FACET_BOTTOM_PERCENT));

		return CGRectMake(new_x, new_y, new_width, new_height);
	}
	
	private var fontSizeCache = [String: CGFloat]();

	func drawText(context: CGContextRef, text: String, var attributes: [String: AnyObject], targetRect: CGRect)
	{
		let options: NSStringDrawingOptions = [.UsesLineFragmentOrigin, .UsesFontLeading]//, .UsesDeviceMetrics];

		var actRect: CGRect;

		let fontSize: CGFloat;
		
		let key: String = makeKey(text);
		
		if let ifontSize = fontSizeCache[key]
		{
			fontSize = ifontSize;
		}
		else
		{
			var size: CGFloat = 2;
			
			repeat
			{
				attributes[NSFontAttributeName] = UIFont.systemFontOfSize(size++);
			
				let attributedString = NSAttributedString(string: text as String, attributes: attributes)
			
				actRect = CGRectIntegral(attributedString.boundingRectWithSize(
					CGSizeMake(300.0, CGFloat.max),
					options: options,
					context: nil
					));
			}
			while actRect.size.width <= targetRect.size.width && actRect.size.height <= targetRect.size.height
			
			fontSize = size - 1;
			fontSizeCache[key] = fontSize;
		}
		
		attributes[NSFontAttributeName] = UIFont.systemFontOfSize(fontSize);
		let attributedString = NSAttributedString(string: text as String, attributes: attributes)

		//let line: CTLineRef = CTLineCreateWithAttributedString(attributedString);
		//let bounds: CGRect = CTLineGetBoundsWithOptions(line, CTLineBoundsOptions.UseGlyphPathBounds);
		
		actRect = CGRectIntegral(attributedString.boundingRectWithSize(
			CGSizeMake(300.0, CGFloat.max),
			options: options,
			context: nil
			));

		actRect.origin.x = 0.0;
		actRect.origin.y = 0.0;

		let actX: CGFloat = (targetRect.size.width - actRect.size.width) / 2;
		let actY: CGFloat = (targetRect.size.height - actRect.size.height) / 2;
		
		CGContextSaveGState(context);

		//CGContextStrokeRect(context, targetRect);
		//CGContextStrokeRect(context, actRect);

		//CGContextTranslateCTM(context, 0, actRect.size.height);
		//CGContextScaleCTM(context, 1.0, -1.0);
		
		// y: Add font.descender (its a negative value) to align the text at the baseline
		let glyphRect = CGRect(x: actX, y: actY + (attributes[NSFontAttributeName] as! UIFont).descender, width: ceil(actRect.size.width), height: ceil(actRect.size.height))

		//glyphRect.origin.x = 0
		//glyphRect.origin.y = 0

		attributedString.drawAtPoint(glyphRect.origin);
		
		//let frameSetter = CTFramesetterCreateWithAttributedString(attributedString)
		//let frame       = CTFramesetterCreateFrame(frameSetter, CFRange(location: 0, length: attributedString.length), textPath, nil)

		//print("Target rect \(targetRect).\nactual Rect \(actRect).\nglyphRect \(glyphRect) size \(size)");
		
		//CTFrameDraw(frame, context)

		CGContextRestoreGState(context);
	}
	
	func makeKey(text: String) -> String
	{
		if(UIDeviceOrientationIsLandscape(UIDevice.currentDevice().orientation))
		{
			return text + ".Landscape";
		}
		else
		{
			return text + ".Portrait";
		}
	}
}