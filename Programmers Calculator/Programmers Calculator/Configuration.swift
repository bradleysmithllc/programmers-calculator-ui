//
//  Configuration.swift
//  Programmers Calculator
//
//  Created by Bradley Smith on 10/25/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

public class Configuration
{
	private let _activeProfile: Profile = DefaultProfile();

	private let _landscapePerspective: Perspective = DefaultPerspective(path: "defaultLandscapePerspective");
	private let _portraitPerspective: Perspective = DefaultPerspective(path: "defaultPortraitPerspective");
	
	private var landscape: Bool = false;

	public func landscape(l: Bool)
	{
		landscape = l;
	}

	public func activePerspective() -> Perspective!
	{
		if (landscape)
		{
			return _landscapePerspective;
		}
		else
		{
			return _portraitPerspective;
		}
	}

	public func activeProfile() -> Profile!
	{
		return _activeProfile;
	}
}