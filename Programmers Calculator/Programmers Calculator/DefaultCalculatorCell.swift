//
//  DefaultCell.swift
//  Programmers Calculator
//
//  Created by Bradley Smith on 10/26/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

public class DefaultCalculatorCell: CalculatorCell
{
	public let _layers: [Layer: (mnemonic: String, buttonType: ButtonType, extendsUp: Bool, extendsDown: Bool, extendsLeft: Bool, extendsRight: Bool)];

	public init(layers: [Layer: (mnemonic: String, buttonType: ButtonType, extendsUp: Bool, extendsDown: Bool, extendsLeft: Bool, extendsRight: Bool)])
	{
		self._layers = layers;
	}
	
	public func layers() -> [Layer]
	{
		return Array(_layers.keys);
	}

	public func mnemonic(layer: Layer) -> String
	{
		if let cell = _layers[layer]
		{
			return cell.mnemonic;
		}
		
		return "INV";
	}

	public func buttonType(layer: Layer) -> ButtonType
	{
		if let cell = _layers[layer]
		{
			return cell.buttonType;
		}
		
		return ButtonType.placeholder;
	}

	public func extendsUp(layer: Layer) -> Bool
	{
		if let cell = _layers[layer]
		{
			return cell.extendsUp;
		}
		
		return false;
	}
	
	public func extendsDown(layer: Layer) -> Bool
	{
		if let cell = _layers[layer]
		{
			return cell.extendsDown;
		}
		
		return false;
	}
	
	public func extendsLeft(layer: Layer) -> Bool
	{
		if let cell = _layers[layer]
		{
			return cell.extendsLeft;
		}
		
		return false;
	}
	
	public func extendsRight(layer: Layer) -> Bool
	{
		if let cell = _layers[layer]
		{
			return cell.extendsRight;
		}
		
		return false;
	}
}