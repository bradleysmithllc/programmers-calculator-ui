//
//  DefaultProfile.swift
//  Programmers Calculator
//
//  Created by Bradley Smith on 10/25/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation
import UIKit

public class DefaultProfile: JSONProfile
{
	public init()
	{
		do
		{
			try super.init(pathName: "defaultProfile");
		}
		catch
		{
			exit(1);
		}
	}
}