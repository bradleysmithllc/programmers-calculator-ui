//
//  Profile.swift
//  Programmers Calculator
//
//  Created by Bradley Smith on 10/25/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation
import UIKit

public enum ButtonType: String
{
	case operation = "operation";
	case operand = "operand";
	case placeholder = "placeholder";

	case normal = "normal";
	case function_a = "function_a";
	case function_b = "function_b";
	case function_c = "function_c";
	case function_d = "function_d";
	case display_x = "display_x";

	
	public static let allValues = [operation, operand, placeholder, normal, function_a, function_b, function_c, function_d, display_x];
	public static let mapValues = [normal.rawValue: normal, operation.rawValue: operation, operand.rawValue: operand, placeholder.rawValue: placeholder, function_a.rawValue: function_a, function_b.rawValue: function_b, function_c.rawValue: function_c, function_d.rawValue: function_d, display_x.rawValue: display_x];
}

public protocol Profile
{
	func buttonColor(buttonType buttonType: ButtonType, layer: Layer) -> UIColor;
	func buttonPressedColor(buttonType buttonType: ButtonType, layer: Layer) -> UIColor;
	func lineColor(buttonType buttonType: ButtonType, layer: Layer) -> UIColor;

	func activeMnemonicColor(buttonType buttonType: ButtonType, layer: Layer) -> UIColor;
	func activeMnemonicColorPressed(buttonType buttonType: ButtonType, layer: Layer) -> UIColor;

	func inactiveMnemonicColor(buttonType buttonType: ButtonType, layer: Layer) -> UIColor;
	func inactiveMnemonicColorPressed(buttonType buttonType: ButtonType, layer: Layer) -> UIColor;
}