//
//  Perspective.swift
//  Programmers Calculator
//
//  Created by Bradley Smith on 10/25/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

public enum Layer: String
{
	case normal = "normal";
	case function_a = "function_a";
	case function_b = "function_b";
	case function_c = "function_c";
	case function_d = "function_d";

	public static let allValues = [normal, function_a, function_b, function_c, function_d];
	public static let mapValues = [normal.rawValue: normal, function_a.rawValue: function_a, function_b.rawValue: function_b, function_c.rawValue: function_c, function_d.rawValue: function_d];
}

public protocol CalculatorCell
{
	func layers() -> [Layer];

	func mnemonic(layer: Layer) -> String;
	func buttonType(layer: Layer) -> ButtonType;

	func extendsUp(layer: Layer) -> Bool;
	func extendsDown(layer: Layer) -> Bool;
	func extendsLeft(layer: Layer) -> Bool;
	func extendsRight(layer: Layer) -> Bool;
}

public protocol Perspective
{
	func rows() -> Int;
	func columns() -> Int;

	func cell(row row: Int, column: Int) -> CalculatorCell;
}