//
//  DefaultPerspective.swift
//  Programmers Calculator
//
//  Created by Bradley Smith on 10/26/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

public class DefaultPerspective: Perspective
{
	private var _rows: Array<Array<MutableCalculatorCell>> = [[MutableCalculatorCell]]();

	public init(path: String)
	{
		do
		{
			if let file = NSBundle(forClass:DefaultPerspective.self).pathForResource(path, ofType: "json") {
				let data = NSData(contentsOfFile: file)!

				let parsedObject: AnyObject? = try NSJSONSerialization.JSONObjectWithData(data,
					options: NSJSONReadingOptions.AllowFragments)
				
				// the top-level is layer-names
				let layers: NSDictionary = parsedObject as! NSDictionary;
				
				for layerName: Layer in Layer.allValues
				{
					// not each layer need be present
					let layerRaw = layers[layerName.rawValue];

					if (layerRaw != nil)
					{
						let layer = layerRaw as! NSDictionary;

						// this is another dictionary with one entry: rows - array
						let rows : NSArray = layer["rows"] as! NSArray;

						var rowNum: Int = 0;

						for row in rows
						{
							if (_rows.count <= rowNum)
							{
								_rows.append([MutableCalculatorCell]());
							}

							let columns : NSArray = row as! NSArray;
							
							// get the corresponding data row
							var columnNum: Int = 0;

							for column in columns
							{
								if (_rows[rowNum].count <= columnNum)
								{
									_rows[rowNum].append(MutableCalculatorCell());
								}

								let extendsUp: Bool = DefaultPerspective.getBoolean(column["extendsUp"] as? String);
								let extendsRight: Bool = DefaultPerspective.getBoolean(column["extendsRight"] as? String);
								let extendsLeft: Bool = DefaultPerspective.getBoolean(column["extendsLeft"] as? String);
								let extendsDown: Bool = DefaultPerspective.getBoolean(column["extendsDown"] as? String);

								// get properties
								let bType: String = column["buttonType"] as! String;
								let buttonType: ButtonType = ButtonType.mapValues[bType]!;

								let mnemonic: String = column["id"] as! String;

								// record it
								_rows[rowNum][columnNum].addLayer(
									layerName,
									mnemonic: mnemonic,
									buttonType: buttonType,
									extendsUp: extendsUp,
									extendsRight: extendsRight,
									extendsDown: extendsDown,
									extendsLeft: extendsLeft
								);
								
								columnNum++;
							}
							
							rowNum++;
						}
					}
				}
			} else {
				print("error");
				_rows = [[MutableCalculatorCell]]();
			}
		}
		catch
		{
			_rows = [[MutableCalculatorCell]]();
		}
	}

	private static func getBoolean(any: String?) -> Bool
	{
		if let b: String = any
		{
			return b == "true"
		}

		return false;
	}

	public func rows() -> Int {
		return _rows.count;
	}

	public func columns() -> Int {
		return _rows[0].count;
	}

	public func cell(row row: Int, column: Int) -> CalculatorCell
	{
		return _rows[row][column];
	}
}