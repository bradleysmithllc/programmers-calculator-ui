//
//  DefaultCell.swift
//  Programmers Calculator
//
//  Created by Bradley Smith on 10/26/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

public class Cell
{
	let mnemonic: String;
	let buttonType: ButtonType;
	let extendsUp: Bool;
	let extendsDown: Bool;
	let extendsLeft: Bool;
	let extendsRight: Bool;
	
	init(
		mnemonic: String, buttonType: ButtonType,
		extendsUp: Bool,
		extendsDown: Bool,
		extendsLeft: Bool,
		extendsRight: Bool
	)
	{
		self.mnemonic = mnemonic;
		self.buttonType = buttonType;
		self.extendsUp = extendsUp;
		self.extendsRight = extendsRight;
		self.extendsDown = extendsDown;
		self.extendsLeft = extendsLeft;
	}
}

public class MutableCalculatorCell: CalculatorCell
{
	private var _layers: [Layer: Cell] = [Layer: Cell]();
	private var activeLayers: [Layer] = [Layer]();

	public func addLayer(
		layerName: Layer,
		mnemonic: String,
		buttonType: ButtonType,
		extendsUp: Bool,
		extendsRight: Bool,
		extendsDown: Bool,
		extendsLeft: Bool
	)
	{
		let tuple = Cell(mnemonic: mnemonic, buttonType: buttonType, extendsUp: extendsUp, extendsDown: extendsDown, extendsLeft: extendsLeft, extendsRight: extendsRight);
		
		if (!activeLayers.contains(layerName))
		{
			activeLayers.append(layerName);
		}

		_layers[layerName] = tuple;
	}

	public func layers() -> [Layer]
	{
		return activeLayers;
	}

	public func mnemonic(layer: Layer) -> String
	{
		if let cell = _layers[layer]
		{
			return cell.mnemonic;
		}
		
		return "INV";
	}

	public func buttonType(layer: Layer) -> ButtonType
	{
		if let cell = _layers[layer]
		{
			return cell.buttonType;
		}
		
		return ButtonType.placeholder;
	}

	public func extendsUp(layer: Layer) -> Bool
	{
		if let cell = _layers[layer]
		{
			return cell.extendsUp;
		}
		
		return false;
	}
	
	public func extendsDown(layer: Layer) -> Bool
	{
		if let cell = _layers[layer]
		{
			return cell.extendsDown;
		}
		
		return false;
	}
	
	public func extendsLeft(layer: Layer) -> Bool
	{
		if let cell = _layers[layer]
		{
			return cell.extendsLeft;
		}
		
		return false;
	}
	
	public func extendsRight(layer: Layer) -> Bool
	{
		if let cell = _layers[layer]
		{
			return cell.extendsRight;
		}
		
		return false;
	}
}