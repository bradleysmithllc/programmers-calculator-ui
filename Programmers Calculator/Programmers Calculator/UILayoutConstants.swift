//
//  LayoutConstants.swift
//  Programmers Calculator
//
//  Created by Bradley Smith on 11/3/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation
import UIKit

public class UILayoutConstants
{
	/**For the normal facet, I.E. the curent mnemonic, this is the
	   percentage height of the total cell height which defines the top line.
	  */
	public static let NORMAL_FACET_TOP_BAR_PERCENT = CGFloat(0.30);

	/**For the normal facet, I.E. the current mnemonic, this is the
	   Percentage height of the total cell height the label can occupy
	*/
	public static let NORMAL_FACET_BOTTOM_PERCENT = CGFloat(0.20);
	
	/**For the normal facet, I.E. the current mnemonic, this is the
		 percentage width that represents the minimum margin on the right side.
	*/
	public static let NORMAL_FACET_RIGHT_MARGIN = CGFloat(0.25);
	
	/**For the normal facet, I.E. the current mnemonic, this is the
		percentage width that represents the minimum margin on the left side.
	*/
	public static let NORMAL_FACET_LEFT_MARGIN = CGFloat(0.25);
}