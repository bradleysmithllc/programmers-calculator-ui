//
//  DefaultProfile.swift
//  Programmers Calculator
//
//  Created by Bradley Smith on 10/25/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation
import UIKit

public class JSONProfile: Profile
{
	private var profileData: [Layer: [ButtonType: [String: UIColor]]] = [Layer: [ButtonType: [String: UIColor]]]();
	
	public init(pathName: String) throws
	{
		if let file = NSBundle(forClass:DefaultPerspective.self).pathForResource(pathName, ofType: "json") {
			let data = NSData(contentsOfFile: file)!

			let parsedObject: AnyObject? = try NSJSONSerialization.JSONObjectWithData(data,
				options: NSJSONReadingOptions.AllowFragments)

			// the top-level is layer-names
			let layers: NSDictionary = parsedObject as! NSDictionary;

			for (layerName, layer) in layers
			{
				print("\(layerName)");

				print(profileData);
				let layerIns: Layer = Layer.mapValues[layerName as! String]!;
				
				profileData[layerIns] = [ButtonType: [String: UIColor]]();
				print(profileData);

				// next level is button types
				let buttonTypes: NSDictionary = layer as! NSDictionary;

				for (buttonTypeName, type) in buttonTypes
				{
					print("\(buttonTypeName)");
					
					let btn = buttonTypeName as! String;
					
					let bt = ButtonType.mapValues[btn]!;

					print("ButtonTypeName \(bt) profile data \(profileData)");
					profileData[layerIns]![bt] = [String: UIColor]();
					print(profileData);

					// next level is selectors with color values
					let selectors: NSDictionary = type as! NSDictionary;
					
					for (selectorName, selectorValue) in selectors
					{
						print("\(selectorName)");
						
						let redFl: CGFloat;
						let greenFl: CGFloat;
						let blueFl: CGFloat;
						let alphaFl: CGFloat;

						// color values
						let colors: NSDictionary = selectorValue as! NSDictionary;

						// get R, G, B, and A
						if let n: Double = colors["red"] as? Double {
							redFl = CGFloat(n / 255.0);
						}
						else
						{
							redFl = CGFloat(0.0);
						}

						if let n: Double = colors["green"] as? Double {
							greenFl = CGFloat(n / 255.0)
						}
						else
						{
							greenFl = CGFloat(0.0);
						}
						
						if let n: Double = colors["blue"] as? Double {
							blueFl = CGFloat(n / 255.0)
						}
						else
						{
							blueFl = CGFloat(0.0);
						}
						
						if let n: Double = colors["alpha"] as? Double {
							alphaFl = CGFloat(n)
						}
						else
						{
							alphaFl = CGFloat(0.0);
						}

						print("\(redFl) \(greenFl) \(blueFl) \(alphaFl)");

						profileData[layerIns]![bt]![selectorName as! String] = UIColor(red: redFl, green: greenFl, blue: blueFl, alpha: alphaFl);
					}
				}
			}
		}
		
		print("\(profileData)");
	}
	
	private func color(
		layer layer: Layer,
		buttonType: ButtonType,
		selector: String
	) -> UIColor
	{
		if let l = profileData[layer]
		{
			if let bt = l[buttonType]
			{
				if let color = bt[selector]
				{
					return color;
				}
			}
		}

		return UIColor.blackColor();
	}

	public func buttonColor(buttonType buttonType: ButtonType, layer: Layer) -> UIColor
	{
		return color(layer: layer, buttonType: buttonType, selector: "buttonColor");
	}
	
	public func buttonPressedColor(buttonType buttonType: ButtonType, layer: Layer) -> UIColor
	{
		return color(layer: layer, buttonType: buttonType, selector: "buttonPressedColor");
	}
	
	public func lineColor(buttonType buttonType: ButtonType, layer: Layer) -> UIColor
	{
		return color(layer: layer, buttonType: buttonType, selector: "lineColor");
	}
	
	public func activeMnemonicColor(buttonType buttonType: ButtonType, layer: Layer) -> UIColor
	{
		return color(layer: layer, buttonType: buttonType, selector: "activeMnemonicColor");
	}
	
	public func activeMnemonicColorPressed(buttonType buttonType: ButtonType, layer: Layer) -> UIColor
	{
		return color(layer: layer, buttonType: buttonType, selector: "activeMnemonicColorPressed");
	}
	
	public func inactiveMnemonicColor(buttonType buttonType: ButtonType, layer: Layer) -> UIColor
	{
		return color(layer: layer, buttonType: buttonType, selector: "inactiveMnemonicColor");
	}
	
	public func inactiveMnemonicColorPressed(buttonType buttonType: ButtonType, layer: Layer) -> UIColor
	{
		return color(layer: layer, buttonType: buttonType, selector: "inactiveMnemonicColorPressed");
	}
}