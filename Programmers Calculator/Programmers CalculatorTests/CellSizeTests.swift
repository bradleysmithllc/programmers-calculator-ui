//
//  Programmers_CalculatorTests.swift
//  Programmers CalculatorTests
//
//  Created by Bradley Smith on 10/21/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import Programmers_Calculator

class CellSizeTests: XCTestCase {
	class func makeRect(originX originX: Double, originY: Double, width: Double, height: Double) -> CGRect
	{
		let fixX = CGFloat(originX);
		let fixY = CGFloat(originY);
		let cPoint = CGPoint(x: fixX, y: fixY);
		
		return CGRect(origin: cPoint, size: CGSize(width: width, height: height));
	}
	
	func testOneCell() {
		let resultRect = CalculatorGridView.calculateCellSize(
			viewRect: CellSizeTests.makeRect(originX: 0, originY: 0, width: 12, height: 10),
			rows: 1,
			columns: 1
		);
		
		XCTAssertEqual(0, resultRect.origin.x);
		XCTAssertEqual(0, resultRect.origin.y);
		XCTAssertEqual(10, resultRect.height);
		XCTAssertEqual(12, resultRect.width);
	}

	func testTwoByTwo() {
		let sourceRect = CalculatorGridView.calculateCellSize(
			viewRect: CellSizeTests.makeRect(originX: 0, originY: 0, width: 12, height: 10),
			rows: 1,
			columns: 1
		);
		
		let resultRect = CalculatorGridView.calculateCellSize(viewRect: sourceRect, rows: 2, columns: 2);
		
		XCTAssertEqual(0, resultRect.origin.x);
		XCTAssertEqual(0, resultRect.origin.y);
		XCTAssertEqual(5, resultRect.height);
		XCTAssertEqual(6, resultRect.width);
	}
}