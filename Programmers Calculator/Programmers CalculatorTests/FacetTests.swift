//
//  Programmers_CalculatorTests.swift
//  Programmers CalculatorTests
//
//  Created by Bradley Smith on 10/21/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import Programmers_Calculator

class FacetTests: XCTestCase {
	func testEmpty()
	{
		let rect = CalculatorGridView.makeNormalFacetTextRect(CGRectMake(0, 0, 0, 0));

		XCTAssertEqual(0, rect.origin.x);
		XCTAssertEqual(0, rect.origin.y);
		XCTAssertEqual(0, rect.size.width);
		XCTAssertEqual(0, rect.size.height);
	}

	func testSingleCell()
	{
		let rect = CalculatorGridView.makeNormalFacetTextRect(CGRectMake(0, 0, 1.0, 1.0));
		
		XCTAssertEqual(0.25, rect.origin.x);
		XCTAssertEqual(0.30, rect.origin.y);
		XCTAssertEqual(0.5, rect.size.width);
		XCTAssertEqual(0.5, rect.size.height);
	}
}