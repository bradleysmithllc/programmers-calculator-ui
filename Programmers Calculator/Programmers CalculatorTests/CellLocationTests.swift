//
//  Programmers_CalculatorTests.swift
//  Programmers CalculatorTests
//
//  Created by Bradley Smith on 10/21/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import Programmers_Calculator

class CellLocationTests: XCTestCase {
	func testOneCell() {
		let sourceRect = CellSizeTests.makeRect(originX: 0, originY: 0, width: 12, height: 10);

		let resultRect = CalculatorGridView.calculateCellRect(
			cellRect: sourceRect,
			row: 0,
			column: 0
		);

		XCTAssertEqual(0, resultRect.origin.x);
		XCTAssertEqual(0, resultRect.origin.y);
		XCTAssertEqual(10, resultRect.height);
		XCTAssertEqual(12, resultRect.width);
	}

	func testTwoByTwo() {
		let sourceRect = CellSizeTests.makeRect(originX: 0, originY: 0, width: 12, height: 10);
		
		var resultRect = CalculatorGridView.calculateCellRect(
			cellRect: sourceRect,
			row: 0,
			column: 0
		);
		
		XCTAssertEqual(0, resultRect.origin.x);
		XCTAssertEqual(0, resultRect.origin.y);
		XCTAssertEqual(10, resultRect.height);
		XCTAssertEqual(12, resultRect.width);
		
		resultRect = CalculatorGridView.calculateCellRect(
			cellRect: sourceRect,
			row: 0,
			column: 1
		);
		
		XCTAssertEqual(12, resultRect.origin.x);
		XCTAssertEqual(0, resultRect.origin.y);
		XCTAssertEqual(10, resultRect.height);
		XCTAssertEqual(12, resultRect.width);
		
		resultRect = CalculatorGridView.calculateCellRect(
			cellRect: sourceRect,
			row: 1,
			column: 0
		);
		
		XCTAssertEqual(0, resultRect.origin.x);
		XCTAssertEqual(10, resultRect.origin.y);
		XCTAssertEqual(10, resultRect.height);
		XCTAssertEqual(12, resultRect.width);
		
		resultRect = CalculatorGridView.calculateCellRect(
			cellRect: sourceRect,
			row: 1,
			column: 1
		);

		XCTAssertEqual(12, resultRect.origin.x);
		XCTAssertEqual(10, resultRect.origin.y);
		XCTAssertEqual(10, resultRect.height);
		XCTAssertEqual(12, resultRect.width);
	}
}