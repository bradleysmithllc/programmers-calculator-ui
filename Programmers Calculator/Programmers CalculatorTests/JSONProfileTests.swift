//
//  Programmers_CalculatorTests.swift
//  Programmers CalculatorTests
//
//  Created by Bradley Smith on 10/21/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import Programmers_Calculator

class JSONProfileTests: XCTestCase {
	func testDefaultProfile() {
		let profile: JSONProfile = DefaultProfile();

		let clr = profile.lineColor(buttonType: ButtonType.operand, layer: Layer.function_a);
		let x = UIColor.blackColor();
	
		//XCTAssertEqual(x, clr);
		let _ = profile.buttonColor(buttonType: ButtonType.operand, layer: Layer.normal);
	}

	func testInit() {
		do
		{
			let profile: JSONProfile = try JSONProfile(pathName: "testProfile");
			XCTAssertEqual(UIColor.blackColor(), profile.lineColor(buttonType: ButtonType.operand, layer: Layer.function_a));
		}
		catch
		{
			XCTFail("\(error)");
		}
	}
	
	func testEmptyProfile() {
		do
		{
			let profile: JSONProfile = try JSONProfile(pathName: "missingProfile");
			XCTAssertEqual(UIColor.blackColor(), profile.lineColor(buttonType: ButtonType.operand, layer: Layer.function_a));
		}
		catch
		{
			XCTFail("\(error)");
		}
	}
	
	func tesFails()
	{
		var map = [String: [String: String]]();
		var subMap = [String: String]();
		map["map"] = subMap;
		subMap["1"] = "2";
		XCTAssertEqual(1,  map.count);
		XCTAssertEqual(1,  map["map"]!.count);  // << Fails
		XCTAssertEqual("2", map["map"]!["1"]);  // << Fails
	}
	
	func tesWorks()
	{
		var map: Dictionary = [String: [String: String]]();
		map["map"]!["1"] = "2";
		XCTAssertEqual(1,  map["map"]!.count);
		XCTAssertEqual("2", map["map"]!["1"]);
	}
}